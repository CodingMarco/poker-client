#pragma once

#include <QMainWindow>
#include <chrono>
#include <thread>
#include <QResizeEvent>
#include <QList>
#include "abfrage.h"
#include "chat.h"
#include <QKeyEvent>
#ifdef _WIN32
	#include <windows.h>
#endif

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(PokerDatabase* m_db, abfrage* abfrageptr, QWidget *parent = nullptr);
	~MainWindow();

private slots:
	void on_actionBeenden_triggered();
	void on_cmdChatSend_clicked();
	void mainEventLoop();
    void on_cmdFold_clicked();
	void on_cmdRaise_clicked();
    void on_cmdCall_clicked();
	void on_yourTurnChanged(bool itsYourTurn);

private:
	Ui::MainWindow *ui;
	abfrage* m;
	Chat chat;
	QPixmap getScaledImage(QString filePath);
	QString getFormattetLabelText(QString title, int value);
	int toNearestFive(int i);
	void resizeEvent(QResizeEvent* ev);
	void closeEvent(QCloseEvent* ev);

    void keyReleaseEvent(QKeyEvent* ev);
};
