#ifndef ANMELDEWINDOW_H
#define ANMELDEWINDOW_H

#include <QDialog>
#include "abfrage.h"
#include <iostream>
#include <QMessageBox>

namespace Ui {
class AnmeldeWindow;
}

class AnmeldeWindow : public QDialog
{
    Q_OBJECT
    abfrage* m;

public:
	explicit AnmeldeWindow(abfrage* abfrageptr, QWidget *parent = nullptr);
    ~AnmeldeWindow();
	bool loginSucccessful = false;

private slots:
	void on_cmdOk_clicked();
	void on_cmdCancel_clicked();

private:
    Ui::AnmeldeWindow *ui;
};

#endif // ANMELDEWINDOW_H
