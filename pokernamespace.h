#pragma once
#include <QString>

namespace Poker
{
	enum Farbe
	{
		Pik,
		Herz,
		Kreuz,
		Karo
	};

	enum Karte
	{
		Eins = 1,
		Zwei,
		Drei,
		Vier,
		Fuenf,
		Sechs,
		Sieben,
		Acht,
		Neun,
		Zehn,
		Bube,
		Dame,
		Koenig,
		Ass
	};
}

struct ChatMessage
{
	int playerID;
	QString message;
	QString color;
};

struct Player
{
	int id;
	QString name;
	int chips;
	int toCall;
	bool hisTurn;
	bool folded;
};
