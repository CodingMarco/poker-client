#include "pokerdatabase.h"

PokerDatabase::PokerDatabase(QObject *parent) : QObject(parent)
{
	db = QSqlDatabase::addDatabase("QMYSQL");
	db.setConnectOptions("MYSQL_OPT_CONNECT_TIMEOUT=5");
	db.setHostName("www.codingmarco.cf");
	db.setDatabaseName("poker");
	db.setUserName("pokerUser");
	db.setPassword("istjawitzig");
	if(db.open())
	{
		qDebug() << "Database wurde verbunden!";
		query = new QSqlQuery(db);
		write = new QSqlQuery(db);
	}
	else
	{
		QMessageBox::critical(nullptr, "Datenbankfehler", db.lastError().text());
		exit(EXIT_FAILURE);
	}
}

PokerDatabase::~PokerDatabase()
{
	delete query;
	delete write;
}
