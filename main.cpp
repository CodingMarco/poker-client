#include <QApplication>

#include "mainwindow.h"
#include "anmeldewindow.h"
#include "wartewindow.h"
#include "pokerdatabase.h"

int main(int argc, char *argv[])
{    
	QApplication a(argc, argv);

	PokerDatabase db;
	abfrage mainAbfrage(&db);

	AnmeldeWindow anmeldeFenster(&mainAbfrage);
	anmeldeFenster.exec();

	if(!anmeldeFenster.loginSucccessful)
		exit(EXIT_FAILURE);

	WarteWindow warteFenster(&mainAbfrage);
	warteFenster.exec();

	MainWindow mainWindow(&db, &mainAbfrage);
	mainWindow.show();


	return a.exec();
}
