#pragma once
#include <QObject>
#include <QString>
#include <QtSql>
#include <QMessageBox>

class PokerDatabase : public QObject
{
	Q_OBJECT
private:
	QSqlDatabase db;

public:
	QSqlQuery* query;
	QSqlQuery* write;
	explicit PokerDatabase(QObject *parent = nullptr);
	~PokerDatabase();

signals:

public slots:
};
