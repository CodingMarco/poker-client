#pragma once
#include <QString>
#include <QtSql>
#include <iostream>
#include <QVariant>
#include <QList>
#include <pokernamespace.h>
#include "pokerdatabase.h"

class abfrage  : public QObject
{
	Q_OBJECT

private:
	PokerDatabase* db;
	Player thisPlayer;

public:
    std::vector<QString> Karten;

	explicit abfrage(PokerDatabase* m_db, QObject *parent = nullptr);
	~abfrage();

	bool joinGame(QString name);
	bool registerUser(QString name, QString pwdHash);

    //work in Progress: Actionen Funktionen
    bool foldAction();
    bool callcheckAction();
    bool betAction(int bet);

	// Getter
	QList<QString> getCards();
	QList<Player> getPlayersData();
	QString getNameByID(int id);
	int getPot();
	bool accountExists(QString name, QString pwdHash);
	bool gameStarted();
	bool accountNameExists(QString name);
	int getProzent();
	bool ControllerOn();

	// Getter for this player's data
    QString getButtonText();
	int getID() { return thisPlayer.id; }
	QString getName() { return thisPlayer.name; }
	bool getFolded()  { return thisPlayer.folded; }
	int getCapital()  { return thisPlayer.chips; }
	bool getYourTurn()   { return thisPlayer.hisTurn; }
	int getToCall()   { return thisPlayer.toCall; }

signals:
	void yourTurnChanged(bool state);
};
