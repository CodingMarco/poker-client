#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QCloseEvent>

MainWindow::MainWindow(PokerDatabase* m_db, abfrage *abfrageptr, QWidget *parent)
	: QMainWindow(parent), ui(new Ui::MainWindow), m(abfrageptr), chat(m_db)
{
	ui->setupUi(this);
	connect(ui->lineEditChat, SIGNAL(returnPressed()), ui->cmdChatSend, SIGNAL(clicked()));
	connect(m, SIGNAL(yourTurnChanged(bool)), this, SLOT(on_yourTurnChanged(bool)));

	QThread::sleep(1); // Wait so that the database contains all players

	auto tblPalette = ui->tblPlayers->palette();
	tblPalette.setColor(QPalette::Active, QPalette::Base, Qt::transparent);
	tblPalette.setColor(QPalette::Inactive, QPalette::Base, Qt::transparent);
	ui->tblPlayers->setPalette(tblPalette);

	QList<Player> playersData = m->getPlayersData();
	ui->tblPlayers->setColumnCount(playersData.size());
	for(int playerIndex = 0; playerIndex < playersData.size(); playerIndex++)
	{
		ui->tblPlayers->setItem(0, playerIndex, new QTableWidgetItem(playersData[playerIndex].name));
		ui->tblPlayers->setItem(1, playerIndex, new QTableWidgetItem(playersData[playerIndex].chips));
		ui->tblPlayers->item(0, playerIndex)->setTextAlignment(Qt::AlignCenter);
		ui->tblPlayers->item(1, playerIndex)->setTextAlignment(Qt::AlignCenter);
	}

	// Kleiner Workaround, damit die Tabelle vollstaendig angezeigt wird...
	ui->tblPlayers->resizeRowsToContents();
	ui->tblPlayers->resizeColumnsToContents();
	int tableWidth = 0, tableHeight = 0;
	for(int i = 0; i < ui->tblPlayers->columnCount(); i++)
		tableWidth += ui->tblPlayers->columnWidth(i);
	for(int i = 0; i < ui->tblPlayers->rowCount(); i++)
		tableHeight += ui->tblPlayers->rowHeight(i);
	ui->tblPlayers->setMinimumSize(tableWidth, tableHeight);
	ui->tblPlayers->resize(tableWidth, tableHeight);

	QTimer* mainEventLoopTimer = new QTimer;
	connect(mainEventLoopTimer, SIGNAL(timeout()), this, SLOT(mainEventLoop()));
	mainEventLoopTimer->start(300);
}

void MainWindow::mainEventLoop()
{
	// Update Players Table + Current Call
	QList<Player> playersData = m->getPlayersData();
	for(int playerIndex = 0; playerIndex < playersData.size(); playerIndex++)
	{
		ui->tblPlayers->item(1, playerIndex)->setText(QString::number(playersData[playerIndex].chips));
		ui->tblPlayers->item(0, playerIndex)->setTextColor(playersData[playerIndex].hisTurn ? "red" : "white");

		// Current Call
		if(playersData[playerIndex].id == m->getID())
		{
			ui->lblCurrentCall->setText(getFormattetLabelText("Current Call", playersData[playerIndex].toCall));
		}
	}
	ui->tblPlayers->resizeColumnsToContents();

	// Bet can't be raised above capital and has to be at least the current call
	ui->spinBoxRaise->setRange(m->getToCall()+5, m->getCapital());

	// Karten
	QList<QString> karten = m->getCards();

	// Mitte-Karten
	karten[0] == "###" ? ui->lblFlop1->setPixmap(getScaledImage(":/res/karten/transparent.png"))
					   : ui->lblFlop1->setPixmap(getScaledImage(QString(":/res/karten/") + karten[0] + ".png"));
	karten[1] == "###" ? ui->lblFlop2->setPixmap(getScaledImage(":/res/karten/transparent.png"))
					   : ui->lblFlop2->setPixmap(getScaledImage(QString(":/res/karten/") + karten[1] + ".png"));
	karten[2] == "###" ? ui->lblFlop3->setPixmap(getScaledImage(":/res/karten/transparent.png"))
					   : ui->lblFlop3->setPixmap(getScaledImage(QString(":/res/karten/") + karten[2] + ".png"));
	karten[3] == "###" ? ui->lblTurn->setPixmap(getScaledImage(":/res/karten/transparent.png"))
					   : ui->lblTurn->setPixmap(getScaledImage(QString(":/res/karten/") + karten[3] + ".png"));
	karten[4] == "###" ? ui->lblRiver->setPixmap(getScaledImage(":/res/karten/transparent.png"))
					   : ui->lblRiver->setPixmap(getScaledImage(QString(":/res/karten/") + karten[4] + ".png"));
	// Handkarten
	karten[5] == "###" ? ui->lblHand1->setPixmap(getScaledImage(":/res/karten/transparent.png"))
					   : ui->lblHand1->setPixmap(getScaledImage(QString(":/res/karten/") + karten[5] + ".png"));
	karten[6] == "###" ? ui->lblHand2->setPixmap(getScaledImage(":/res/karten/transparent.png"))
					   : ui->lblHand2->setPixmap(getScaledImage(QString(":/res/karten/") + karten[6] + ".png"));

	// Pot & Capital
	ui->lblPot->setText(getFormattetLabelText("Pot", m->getPot()));
	ui->lblCurrentCapital->setText(getFormattetLabelText("Current Capital", m->getCapital()));

	// Chat
	QList<ChatMessage> newMessages = chat.getMessages();
	for(ChatMessage x : newMessages)
	{
		if(x.playerID != m->getID())
		{
			ui->textBrowserChat->append("[" + m->getNameByID(x.playerID) + "] >> "
										+ "<span style='color: " + x.color + ";'>" + x.message + "</span>");
		}
	}

    //call etc. Text Veränderung je nachdem wieviel der Call wäre!
    ui->cmdCall->setText(m->getButtonText());
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::on_actionBeenden_triggered()
{
	closeEvent(new QCloseEvent());
}

QPixmap MainWindow::getScaledImage(QString filePath)
{
	int imageWidth = float(this->width() - 50 - ui->lblPot->width() - ui->spacerMitte1->sizeHint().width()
																	- ui->spacerMitte2->sizeHint().width()
																	- ui->spacerMitte3->sizeHint().width()) / 6;
	QImage i(filePath);
	if(imageWidth < 220)
		return QPixmap::fromImage(i.scaledToWidth(imageWidth));
	else
		return QPixmap::fromImage(i);
}

QString MainWindow::getFormattetLabelText(QString title, int value)
{
	return QString("<html><head/><body><p align='center'><span style=' font-size:16pt;'>")
		   + title + ":</span></p><p align='center'>"
			+ QString::number(value) + " $</p></body></html>";
}

int MainWindow::toNearestFive(int i)
{
	int j = i%5;
	return (i - j + (j/5.0 >= 0.5 ? 5 : 0));
}

void MainWindow::resizeEvent(QResizeEvent *ev)
{
	QPixmap background(":/res/gui/background.jpg");
	background = background.scaled(this->size(), Qt::IgnoreAspectRatio);
	QPalette palette;
	palette.setBrush(QPalette::Background, background);
	this->setPalette(palette);

	ui->lblRiver->setPixmap(getScaledImage(":/res/karten/transparent.png"));
	ui->lblTurn->setPixmap(getScaledImage(":/res/karten/transparent.png"));
	ui->lblFlop3->setPixmap(getScaledImage(":/res/karten/transparent.png"));
	ui->lblFlop2->setPixmap(getScaledImage(":/res/karten/transparent.png"));
	ui->lblFlop1->setPixmap(getScaledImage(":/res/karten/transparent.png"));
	ui->lblHand1->setPixmap(getScaledImage(":/res/karten/transparent.png"));
	ui->lblHand2->setPixmap(getScaledImage(":/res/karten/transparent.png"));

	QSize lblSize(this->size().width()/6, this->size().height()/7);
	ui->lblPot->setMinimumSize(lblSize);
	ui->lblCurrentCall->setMinimumSize(lblSize);
	ui->lblCurrentCapital->setMinimumSize(lblSize);
}

void MainWindow::closeEvent(QCloseEvent *ev)
{
	// Damit wird auch das Datenbank Debugfenster geschlossen, das sonst offen bleibt...
	// Hier müsste man dann auch den spieler abmelden...
	QApplication::quit();
}

void MainWindow::on_cmdChatSend_clicked()
{
	if(ui->lineEditChat->text().length() > 0)
	{
		QString text = ui->lineEditChat->text();
		int i = 0b1111110;
		ui->textBrowserChat->append(text.contains(i) ? "[You] >> <span style='color:" + text.split(i)[1]
									+ ";'>" + text.split(i)[0] + "</span>" : "[You] >> " + text);
		chat.sendChatMessage(m->getID(), text.contains(i) ? text.split(i)[0] : text, text.contains(i) ? text.split(i)[1] : "");
		ui->lineEditChat->clear();
	}
}

void MainWindow::on_cmdFold_clicked()
{
	if(m->getYourTurn())
    {
		#ifdef _WIN32
			Beep(600,200); // 300 hz,200 ms
		#endif
        m->foldAction();
    }
    else
    {
		std::cout << '\a';
		std::cout.flush();
    }
}

void MainWindow::on_cmdCall_clicked()
{
	if(m->getYourTurn())
    {
		#ifdef _WIN32
			Beep(600,200); // 300 hz,200 ms
		#endif
		m->callcheckAction();
    }
    else
    {
		std::cout << '\a';
		std::cout.flush();
	}
}

void MainWindow::on_yourTurnChanged(bool itsYourTurn)
{
	if(itsYourTurn)
	{
		ui->spinBoxRaise->setValue(m->getToCall()+5);
	}
	// Buttons enablen / disablen je nach dem ob man dran ist
	ui->cmdCall->setEnabled(itsYourTurn);
	ui->cmdFold->setEnabled(itsYourTurn);
	ui->cmdRaise->setEnabled(itsYourTurn);
	ui->spinBoxRaise->setEnabled(itsYourTurn);
}

void MainWindow::on_cmdRaise_clicked()
{
    #ifdef _WIN32
        Beep(600,200); // 300 hz,200 ms
    #endif
	m->betAction(toNearestFive(ui->spinBoxRaise->value()));
}

void MainWindow::keyReleaseEvent(QKeyEvent *ev)
{
    if(ev->key() == Qt::Key_1)
        this->on_cmdCall_clicked();
    else if(ev->key() == Qt::Key_2)
        this->on_cmdRaise_clicked();
    else if(ev->key() == Qt::Key_3)
        this->on_cmdFold_clicked();
}
