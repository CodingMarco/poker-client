#-------------------------------------------------
#
# Project created by QtCreator 2019-01-15T13:47:36
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = poker-client
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

CONFIG += c++17

SOURCES += \
        main.cpp \
        mainwindow.cpp \
        abfrage.cpp \
        anmeldewindow.cpp \
        wartewindow.cpp \
        chat.cpp \
        pokerdatabase.cpp

HEADERS += \
        mainwindow.h \
        abfrage.h \
        anmeldewindow.h \
        wartewindow.h \
        pokernamespace.h \
        chat.h \
        pokerdatabase.h

FORMS += \
        mainwindow.ui \
        anmeldewindow.ui \
        wartewindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += resources.qrc

DISTFILES += TODO
