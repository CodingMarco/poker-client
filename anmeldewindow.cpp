#include "anmeldewindow.h"
#include "ui_anmeldewindow.h"

AnmeldeWindow::AnmeldeWindow(abfrage* abfrageptr,QWidget *parent) :
	QDialog(parent),
    ui(new Ui::AnmeldeWindow)
{
    m = abfrageptr;
    ui->setupUi(this);
}

AnmeldeWindow::~AnmeldeWindow()
{
    delete ui;
}

void AnmeldeWindow::on_cmdOk_clicked()
{
	QString name = ui->inputName->text();
	// Das S@l! ist ein sogenanntes salt, damit man nicht nach Hashes von Passwoertern wie 1234 bei google suchen kann...
	QString pwdHash = QCryptographicHash::hash((ui->inputPwd->text() + "S@l!").toUtf8(), QCryptographicHash::Md5).toHex();

	// Registrierung
	if(ui->chkRegister->checkState())
    {
        if(!m->accountNameExists(name))
		{
			if(m->registerUser(name, pwdHash))
				QMessageBox::information(this, "Registrierung erfolgreich!", "Du kannst dich jetzt anmelden!");
			else
				QMessageBox::critical(this, "Error", "Bei der Registrierung ist ein Fehler aufgetreten.");
		}
		else
		{
			QMessageBox::critical(this, "Error", "Account existiert bereits!");
			return;
		}
    }
	else
	{
		// Anmeldung
		if(!m->accountExists(name, pwdHash))
		{
			QMessageBox::critical(this, "Error", "Benutzername oder Passwort falsch!");
		}
		else if(!m->ControllerOn())
		{
			QMessageBox::information(this, "Error", "Kein Game gefunden. Bitte versuche es gleich nochmal.");
		}
		else
		{
			if(m->joinGame(ui->inputName->text()))
			{
				loginSucccessful = true;
				this->close();
			}
			else
				QMessageBox::critical(this, "Error", "Beim Eintritt ins Spiel ist ein Fehler aufgetreten.");
		}
	}
}

void AnmeldeWindow::on_cmdCancel_clicked()
{
	if(QMessageBox::question(this, "Bist du dir sicher?", "Willst du das Spiel wirklich verlassen?"))
	{
		exit(EXIT_SUCCESS);
	}
}
