#ifndef WARTEWINDOW_H
#define WARTEWINDOW_H

#include <QDialog>
#include <QShowEvent>
#include <QTimer>
#include "abfrage.h"

namespace Ui {
class WarteWindow;
}

class WarteWindow : public QDialog
{
	Q_OBJECT

public:
	explicit WarteWindow(abfrage* m_abfrage, QWidget *parent = nullptr);
	~WarteWindow();

private slots:
	void on_cmdCancel_clicked();
	void checkOnlinePlayers();

private:
	Ui::WarteWindow *ui;
	abfrage* m;
	QTimer* timer = nullptr;
	void showEvent(QShowEvent* ev);
};

#endif // WARTEWINDOW_H
