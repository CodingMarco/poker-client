#include "wartewindow.h"
#include "ui_wartewindow.h"
#include <QMessageBox>
#include <QDebug>

WarteWindow::WarteWindow(abfrage *m_abfrage, QWidget *parent) :
	QDialog(parent),
	ui(new Ui::WarteWindow)
{
	ui->setupUi(this);
	m = m_abfrage;
}

WarteWindow::~WarteWindow()
{
	if(timer != nullptr)
		timer->stop();
	delete timer;
	delete ui;
}

void WarteWindow::on_cmdCancel_clicked()
{
	if(QMessageBox::question(this, "Bist du dir sicher?", "Willst du das Spiel wirklich verlassen?"))
	{
		exit(EXIT_SUCCESS);
	}
}

void WarteWindow::checkOnlinePlayers()
{
	ui->pbOnline->setValue(m->getProzent());
	if(m->gameStarted())
	{
		this->close();
	}
	else
	{
		return;
	}
}

void WarteWindow::showEvent(QShowEvent *ev)
{
	// Jede Sekunde wird die checkOnlinePlayers() Funktion aufgerufen
	timer = new QTimer(this);
	connect(timer, SIGNAL(timeout()), this, SLOT(checkOnlinePlayers()));
	timer->start(100);
	ev->accept();
}
