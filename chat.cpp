#include "chat.h"

Chat::Chat(PokerDatabase* m_db, QObject *parent)
	: QObject(parent), db(m_db)
{
}

QList<ChatMessage> Chat::getMessages()
{
	QList<ChatMessage> messages;
	QString querystr = QString("SELECT MessageID,SpielerID,Message,Color FROM Chat WHERE MessageID > ")
					 + QString::number(currentChatIndex) + " ORDER BY MessageID";
	db->query->exec(querystr);

	while(db->query->next())
	{
		ChatMessage currentMessage;
		currentMessage.playerID = db->query->value(1).toInt();
		currentMessage.message = db->query->value(2).toString();
		currentMessage.color = db->query->value(3).toString();
		messages.push_back(currentMessage);
	}
	db->query->exec("SELECT MAX(MessageID) FROM Chat WHERE 1");
	db->query->first();
	currentChatIndex = db->query->value(0).toInt();
	return messages;
}

void Chat::sendChatMessage(int id, QString message, QString color)
{
	db->write->prepare("INSERT INTO Chat (MessageID, SpielerID, Message, Color) VALUES (NULL, ?, ?, ?)");
	db->write->addBindValue(id);
	db->write->addBindValue(message);
	db->write->addBindValue(color);
	if(!db->write->exec())
	{
		qDebug() << "Sending message failed!";
	}
}
