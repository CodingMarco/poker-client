#include "abfrage.h"
#include <QMessageBox>
#include <QDebug>

abfrage::abfrage(PokerDatabase* m_db, QObject *parent)
	: QObject(parent), db(m_db)
{
}

abfrage::~abfrage()
{
}

bool abfrage::accountExists(QString name, QString pwdHash)
{
	// Schaut ob ein Eintrag in 'Accounts' existiert
	db->query->prepare("SELECT COUNT(*) FROM Accounts WHERE SpielerName = ? AND Passwort = ?");
	db->query->addBindValue(name);
	db->query->addBindValue(pwdHash);
	db->query->exec();
	while(db->query->next())
    {
		if(db->query->value(0).toInt())
        {
			return true;
        }
    }
    return false;
}

bool abfrage::joinGame(QString name)
{
	// Erstellt einen Eintrag in 'Anmeldung' mit dem Spieler Namen und seinerID
	db->query->prepare("SELECT SpielerID from Accounts where SpielerName = ?");
	db->query->addBindValue(name);
	db->query->exec();
	while(db->query->next())
    {
		thisPlayer.id = db->query->value(0).toInt();
    }
	db->write->prepare("INSERT INTO Anmeldung(SpielerName, SpielerID) VALUES (?,?)");
	db->write->addBindValue(name);
	db->write->addBindValue(thisPlayer.id);
	if(db->write->exec())
        return true;
	else
		return false;
}

bool abfrage::registerUser(QString name, QString pwdHash)
{
	db->write->prepare("INSERT INTO `Accounts`(`SpielerName`, `Passwort`) VALUES (?,?)");
	db->write->addBindValue(name);
	db->write->addBindValue(pwdHash);
	if(db->write->exec())
	{
		return true;
	}
	else
	{
		return false;
	}
}

QList<QString> abfrage::getCards()
{
	// Mitte-Karten: 0-4, Handkarten: 5-6
	QList<QString> karten;
	db->query->exec("SELECT Flop1,Flop2,Flop3,Turn,River FROM Global where 1");
	while(db->query->next())
    {
		for(int i = 0; i < 5; i++)
        {
			if(!db->query->isNull(i))
				karten.push_back(db->query->value(i).toString());
			else
				karten.push_back("###");
        }
    }
	db->query->prepare("SELECT HandKarte1,HandKarte2 FROM Spieler WHERE SpielerID = ?");
	db->query->addBindValue(thisPlayer.id);
	db->query->exec();
	db->query->first();
	if(db->query->isValid())
	{
		karten.push_back(db->query->value(0).toString());
		karten.push_back(db->query->value(1).toString());
	}
	else
	{
		karten.push_back("###");
		karten.push_back("###");
	}
	return karten;
}

QList<Player> abfrage::getPlayersData()
{
	QList<Player> playerData;
	static bool yourTurnBefore = false;

	db->write->exec("SELECT SpielerID,SpielerName,Chips,toCall,YourTurn,folded FROM Spieler WHERE 1 ORDER BY SpielerID");
	while(db->write->next())
	{
		playerData.push_back({db->write->value(0).toInt(),
							  db->write->value(1).toString(),
							  db->write->value(2).toInt(),
							  db->write->value(3).toInt(),
							  db->write->value(4).toBool(),
							  db->write->value(5).toBool()});
		// Update thisPlayer
		if(playerData.last().id == thisPlayer.id)
		{
			yourTurnBefore = getYourTurn();
			thisPlayer = playerData.last();
			if(getYourTurn() != yourTurnBefore)
				emit yourTurnChanged(getYourTurn());
		}
	}
	return playerData;
}

QString abfrage::getNameByID(int id)
{
	db->query->prepare("SELECT SpielerName FROM Accounts WHERE SpielerID = ?");
	db->query->addBindValue(id);
	db->query->exec();
	db->query->first();
	return db->query->value(0).toString();
}

int abfrage::getPot()
{
	db->query->exec("SELECT Pot FROM `Global` WHERE 1");
	while(db->query->next())
    {
		return db->query->value(0).toInt();
    }
    return 0;
}

bool abfrage::gameStarted()
{
    //schaut ob ein passender Spieler schon durch den Controller erstellt wurde. Wenn true kann das SpielFenster erstellt werden!
	db->query->prepare("SELECT COUNT(*) FROM Spieler where SpielerID = ?");
	db->query->addBindValue(thisPlayer.id);
	db->query->exec();
	while(db->query->next())
    {
		if(db->query->value(0).toInt() != 0)
			return true;
		else
			return false;
    }
    return false;
}

bool abfrage::accountNameExists(QString name)
{
	db->query->prepare("SELECT COUNT(*) FROM Accounts WHERE SpielerName = ?");
	db->query->addBindValue(name);
	db->query->exec();
	while(db->query->next())
    {
		if(db->query->value(0).toInt() == 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    return true;
}

int abfrage::getProzent()
{
	db->query->exec("SELECT ProzentGameBuilt from Global where 1");
	while(db->query->next())
		return db->query->value(0).toInt();
    return 0;
}

bool abfrage::ControllerOn()
{
	db->query->exec("SELECT ControllerOn from Global where 1");
	while(db->query->next())
    {
		return db->query->value(0).toBool();
    }
    return false;
}

bool abfrage::foldAction()
{
	if(!thisPlayer.hisTurn)
        return false;

	db->write->prepare("UPDATE Spieler SET folded = true, Action = 1 WHERE SpielerID = ?");
	db->write->addBindValue(thisPlayer.id);
	db->write->exec();

    return true;
}

bool abfrage::callcheckAction()
{
	if(!thisPlayer.hisTurn)
        return false;

	if(getCapital() < thisPlayer.toCall)
        return false;

	db->write->prepare("UPDATE Spieler SET Action = 2 WHERE SpielerID = ?");
	db->write->addBindValue(thisPlayer.id);
	db->write->exec();

    return true;
}

bool abfrage::betAction(int bet)
{
	if(!thisPlayer.hisTurn)
        return false;

	if(bet > getCapital())
        return false;

    if(bet < thisPlayer.toCall && bet < getCapital())
        return false;

	db->write->prepare("UPDATE Spieler SET Action = 3, bet = ? WHERE SpielerID = ?");
	db->write->addBindValue(bet);
	db->write->addBindValue(thisPlayer.id);
	db->write->exec();

    return true;
}

QString abfrage::getButtonText()
{
    if(thisPlayer.toCall == 0)
        return "check [1]";
    else if(thisPlayer.toCall >= thisPlayer.chips)
        return "All-in [1]";
    else
        return "call [1]";
}
