#pragma once

#include <QObject>
#include <QList>
#include <pokernamespace.h>
#include <pokerdatabase.h>


class Chat : public QObject
{
	Q_OBJECT

private:
	int currentChatIndex = 1;
	PokerDatabase* db;

public:
	explicit Chat(PokerDatabase* m_db, QObject *parent = nullptr);
	QList<ChatMessage> getMessages();
	void sendChatMessage(int id, QString message, QString color = "");

};
